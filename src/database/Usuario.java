package database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Usuario {
	
	private String login_funcionario; 
	private String senha_funcionario;
	private String id_funcionario; 
	private String nome_funcionario; 

	
	private String tableName	= "biblioteca.funcionario";
	private String fieldsName 	= "login_funcionario, senha_funcionario, id_funcionario, nome_funcionario";
	private String fieldKey		= "idUsuario";
	
	DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	
	
	public Usuario(String login_funcionario, String senha_funcionario, String id_funcionario, String  nome_funcionario) {
		this.login_funcionario = login_funcionario;
		this.senha_funcionario      = senha_funcionario;
		this.id_funcionario     = id_funcionario;
		this.nome_funcionario     = nome_funcionario;
		
	}
	
	private String[] toArray() {
		return(
			new String[]{
				this.login_funcionario, 
				this.senha_funcionario,
				this.id_funcionario,
				this.nome_funcionario			
			}
		);

	}
	
	public void save() {
		if (this.idUsuario == ""){
			this.idUsuario = "0";
			dbQuery.insert(this.toArray());
		}else{
			dbQuery.update(this.toArray());
		}
	}
	
	public void delete() {
		if (this.idUsuario != ""){
			dbQuery.delete(this.toArray());
		}
	}
	
	public boolean checkLogin() {
		
		ResultSet rs =	dbQuery.select(" (nome = '"+this.nome+"' OR email = '"+this.email+"') AND  senha = '"+this.senha+"' ");
	 	try {
	 		return( rs.next() );
		 /*
			if ( rs.next() ){
			 return( true );
		 	}	else {
			 	return( false );
		 	}
		 */
	 	} catch (SQLException e) {
			e.printStackTrace();
		}
	 	return( false );
	}
	
	public ResultSet listall() {
		return(dbQuery.select(""));
	}
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

}
