package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import database.DBConnection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class TalaInicial extends JFrame {

	private JPanel contentPane;
	private JTextField usuario;
	private JTextField senha;
	private JTextField textField;
	private JPasswordField passwordField;
	private JTextField txtBibliotecaMunicipal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TalaInicial frame = new TalaInicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TalaInicial() {
		setBackground(Color.WHITE);
		setTitle("Tela Inicial");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contentPane.setForeground(SystemColor.menu);
		contentPane.setBackground(SystemColor.menu);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane txtpnLogin = new JTextPane();
		txtpnLogin.setBackground(SystemColor.menu);
		txtpnLogin.setFont(new Font("Yu Gothic Light", Font.PLAIN, 40));
		txtpnLogin.setText("LOGIN");
		txtpnLogin.setBounds(149, 30, 149, 69);
		contentPane.add(txtpnLogin);
		
		usuario = new JTextField();
		usuario.setBackground(SystemColor.menu);
		usuario.setFont(new Font("Yu Gothic Light", Font.PLAIN, 19));
		usuario.setText("Usu\u00E1rio:");
		usuario.setBounds(10, 112, 80, 37);
		contentPane.add(usuario);
		usuario.setColumns(10);
		
		senha = new JTextField();
		senha.setText("Senha:");
		senha.setFont(new Font("Yu Gothic Light", Font.PLAIN, 19));
		senha.setColumns(10);
		senha.setBackground(SystemColor.menu);
		senha.setBounds(10, 178, 80, 37);
		contentPane.add(senha);
		
		textField = new JTextField();
		textField.setFont(new Font("Yu Gothic Light", Font.PLAIN, 19));
		textField.setBounds(100, 110, 116, 37);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(100, 176, 116, 37);
		contentPane.add(passwordField);
		
		JButton btnNewButton = new JButton("ENTRAR");
		/*btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funcionario usuario = new funcionario("", usuario.getText(), txtUsuario.getText(), senha.getText(), "");
				if (usuario.checkLogin()){
					new FrmCadUsuario();
					dispose();
				}else{
					lblMsg.setText("Usu�rio ou Senha Inv�lidos");
				}
				
			}
		});*/
		btnNewButton.setBackground(SystemColor.menu);
		btnNewButton.setFont(new Font("Yu Gothic Light", Font.PLAIN, 16));
		btnNewButton.setBounds(268, 131, 116, 50);
		contentPane.add(btnNewButton);
		
		txtBibliotecaMunicipal = new JTextField();
		txtBibliotecaMunicipal.setFont(new Font("Yu Gothic Light", Font.PLAIN, 13));
		txtBibliotecaMunicipal.setText("Biblioteca Municipal");
		txtBibliotecaMunicipal.setBackground(SystemColor.menu);
		txtBibliotecaMunicipal.setBounds(149, 0, 126, 28);
		contentPane.add(txtBibliotecaMunicipal);
		txtBibliotecaMunicipal.setColumns(10);
	}
}
